﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas
{
    class Venta
    {
        private Cliente cliente;
        private List<Producto> listaproductos;
        private double valor;


    public Venta()
        {
            this.cliente = null;
            this.listaproductos = null;
            this.valor = 0;
        }

        public Cliente getCliente()
        {
            return this.cliente;
        }

        public List<Producto> getListaProductos()
        {
            return this.listaproductos;
        }

        public double getValorVenta()
        {
            return this.valor;
        }

        public void setCliente(Cliente cliente)
        {
            this.cliente = cliente;
        }

        public void setListaProductos(List<Producto> listap)
        {
            this.listaproductos = listap;
        }

        public void setValorVenta(double valor)
        {
            this.valor = valor;
        }

        public void imprimirVenta()
        {
            Console.WriteLine("");
            Console.WriteLine("Venta de "+this.cliente.getNombre() + " "+ this.cliente.getApellido());
            for (int i = 0; i < this.listaproductos.Count(); i++)
            {
                Console.WriteLine(listaproductos[i].getNombreProducto() + "   " + listaproductos[i].getPrecioProducto());
            }
            Console.WriteLine("---------------------");
            Console.WriteLine("Total   =  " + this.getValorVenta());

        }
    }
}
