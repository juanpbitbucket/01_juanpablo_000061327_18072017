﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas
{
    class Cliente
    {
        private string nombre;
        private string apellido;
        private string cedula;
        private string telefono;


        public Cliente()
        {
            this.nombre = "";
            this.apellido = "";
            this.cedula = "";
            this.telefono = "";
        }

        public Cliente(string nombre, string apellido, string cedula, string telefono)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.cedula = cedula;
            this.telefono = telefono;
        }

        public string getNombre()
        {
            return this.nombre;
        }

        public string getApellido()
        {
            return this.apellido;
        }

        public string getCedula()
        {
            return this.cedula;
        }

        public string getTelefono()
        {
            return this.telefono;
        }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public void setApellido(string apellido)
        {
            this.apellido=apellido;
        }

        public void setCedula(string cedula)
        {
            this.cedula=cedula;
        }

        public void setTelefono(string telefono)
        {
            this.telefono=telefono;
        }

    }
}
