﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas
{
    class Producto
    {
        private string nombre;
        private double precio;

        public Producto()
        {
            this.nombre = "";
            this.precio = 0;
        }

        public Producto(string nombre,double precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public string getNombreProducto()
        {
            return this.nombre;
        }

        public double getPrecioProducto()
        {
            return this.precio;
        }

        public void setNombreProducto(string nombre)
        {
            this.nombre = nombre;
        }

        public void setPrecioProducto(double precio)
        {
            this.precio = precio;
        }
    }
}
