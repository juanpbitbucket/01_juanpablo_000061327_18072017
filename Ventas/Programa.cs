﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas
{
    class Programa
    {
        static void Main(string[] args)
        {
            Venta venta = new Venta();

            Console.WriteLine("Ventas DonPacho");
            //Esta linea la edito Santiago Londoño
            Console.WriteLine("Menu Clientes:");
            Cliente cliente = registroCliente();
            venta.setCliente(cliente);
            List<Producto> listaproductos = registroVenta();
            venta.setListaProductos(listaproductos);
            double valorventa = calcularValor(listaproductos);
            venta.setValorVenta(valorventa);
            venta.imprimirVenta();
            Console.Read();

        }



        public static Cliente registroCliente()
        {
            Console.WriteLine("1. Registro del Cliente");
            Console.WriteLine("Ingrese el nombre del cliente");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el apellido del cliente");
            string apellido = Console.ReadLine();
            Console.WriteLine("Ingrese la cedula del cliente");
            string cedula = Console.ReadLine();
            Console.WriteLine("Ingrese el telefono del cliente");
            string telefono = Console.ReadLine();
            Cliente cliente = new Cliente(nombre, apellido, cedula, telefono);

            return cliente;
        }


        public static List<Producto> registroVenta()
        {
            List<Producto> listaproductos = new List<Producto>();

            Console.WriteLine("Registro de los productos de la venta");

            int opcion = 0;
            while (opcion != 2)
            {
                Console.WriteLine("Ingrese una de las siguientes opciones");
                Console.WriteLine("1.Registrar producto");
                Console.WriteLine("2.Finalizar");

                try
                {
                    opcion = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {

                    Console.WriteLine("Ingrese una opcion valida");
                }

                if (opcion == 1)
                {
                    Producto producto = registrarProducto();
                    listaproductos.Add(producto);
                }


            }


            return listaproductos;
        }


        public static Producto registrarProducto()
        {
            Console.WriteLine("Ingrese el nombre del producto");
            string nombre = Console.ReadLine();
            double precio = 0;

            bool ok = false;
            while (ok == false)
            {

                try
                {
                    Console.WriteLine("Ingrese el precio del producto");
                    precio = double.Parse(Console.ReadLine());
                    ok = true;

                }
                catch (Exception)
                {
                    Console.WriteLine("Ingrese un valor valido");
                }
            }

            return new Producto(nombre, precio);
        }


        public static double calcularValor(List<Producto> listap)
        {
            double valorventa = 0;
            for (int i = 0; i < listap.Count(); i++)
            {
                valorventa += listap[i].getPrecioProducto();
            }
            return valorventa;
        }

    }
}
